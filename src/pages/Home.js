import {
    Center, Text, Stack, Tabs,
    TabList,
    Box, Container, VStack, Flex, ChakraProvider, Card, CardBody,
} from '@chakra-ui/react'
import image1 from '../static/image-from-rawpixel-id-6434122-original 1I331_22;248_298.png'
import img2 from '../static/image-from-rawpixel-id-6434112-original 1I331_14;250_299.png'
import img3 from '../static/image-from-rawpixel-id-6434103-original 1I331_32;250_300.png'
import shirt from '../static/image 8.png'
import '../index.css'
import '@fontsource/cormorant-unicase';
import '@fontsource/cormorant-infant'

import HeadTab from '../components/HeadTab'
import { MainCard } from '../components/MainCard'
import ButtonRedirect from '../components/ButtonRedirect'
import Footer from '../components/Footer'


export const Home = () => {
    return (
        <ChakraProvider  >
            <Flex direction={'column'} justifyContent={'center'} alignItems={'center'} position={'fixed'} right={0} top={0} left={0} >
                <Tabs variant="unstyled" defaultIndex={1}  >
                    <TabList >
                        <HeadTab text="О ТАРО" url="about" />
                        <HeadTab text="ГЛАВНАЯ" url="" isSelected={true} />
                        <HeadTab text="ГАДАНИЯ" url="predictions" />
                    </TabList>
                </Tabs>
            </Flex>
            
            <Box  fontSize={24} textColor='#EBCA83'  >
                <VStack>
                    <Container centerContent fontSize={40} as={'b'} marginTop={'202px'} fontFamily={'Cormorant Infant'}>ГАДАНИЯ НА КАРТАХ ТАРО</Container>
                    <Container centerContent fontSize={24} textColor={'#EDF2F4'} textAlign={'center'} fontFamily={'Cormorant Unicase'} h={'101px'} w={'900px'} >

                        <Text h={'87px'} w={'885px'}> Это один из вариантов гадания, оккультная методика, якобы позволяющая путём манипуляции с картами Таро, раскрыть будущие события основываясь на сформулированных вопросах.
                        </Text>
                    </Container>
                </VStack>



                <Center marginTop={'80px'} >
                    <Stack spacing={2} direction='row'  >
                        <MainCard textHead="Использование карт Таро
                                        для гадания и предсказаний"
                            textMini="- это практика, уходящая корнями глубоко в историю человечества."
                            image={image1}
                            imgH='260px'
                            alTextHead="Гадание с помощью этих карт позволяет пролить свет на различные аспекты жизни,"
                            alTextMini="отношений и ситуаций, помогая самому человеку представить, как действия людей и событий влияют на его текущее и будущее состояние." />
                        <MainCard textHead="Таро предлагает  взглянуть не только
                                        на внешние факты,"
                            textMini="но и на внутреннее состояние,
                                        интуитивные ощущения и прозрения."
                            image={img2}
                            imgH='240px'
                            alTextHead="Оно помогает открыть дверь в более глубокое понимание своей жизни и ситуаций, "
                            alTextMini="которые возникают, предоставляя ключи к самопознанию и уважительному отношению к другим." />
                    </Stack>
                </Center>
                <Center marginTop={'90px'}>
                    <Stack spacing={2} direction='row'  >
                        <MainCard textHead="Эта мудрая практика позволяет нам увидеть скрытые аспекты сложных ситуаций,"
                            textMini="используя символы и метафоры, и принимать более  мудрые решения."
                            image={img3}
                            imgH='200px'
                            alTextHead="Таким образом, гадание на картах Таро предлагает прозорливые и вдохновляющие ответы,"
                            alTextMini="помогая найти свет в темные уголки судьбы и преодолеть трудности." />
                        <Card height={'535px'} width={'385px'} position={'static'} bg={'#ffffff00'} border={'#ffffff00'} shadow={'none'} marginRight={'20'}>
                            <CardBody backgroundImage={shirt} backgroundSize={'cover'} >
                                <Center alignItems={'center'} alignContent={'center'} marginTop={'170px'} >
                                    <Text fontFamily={'Cormorant Infant'} textColor={'#EBCA83'} align={'center'} fontSize={32} as='b'>
                                        Совет:
                                    </Text>
                                </Center>
                                <Center >
                                    <Text fontFamily={'Cormorant Unicase'} textColor={'#EDF2F4'} align={'center'} fontSize={20}>
                                        Не рекомендуем слишком часто пользоваться Таро раскладами, так как это может ввести вас в заблуждение.
                                    </Text>
                                </Center>
                            </CardBody>
                        </Card>
                    </Stack>
                </Center>

                <Center marginTop={'132px'}>
                    <ButtonRedirect text="НАЖМИ ЧТОБЫ НАЧАТЬ" url="predictions" />
                </Center>

                <Footer/>

            </Box>
        </ChakraProvider>)
}