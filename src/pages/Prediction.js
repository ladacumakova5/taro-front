import { ChakraProvider, Box, Flex, Tabs, TabList, Stack, Center, Container, VStack, CardBody , Card, Text} from '@chakra-ui/react'
import '../index.css'
import Footer from '../components/Footer'
import HeadTab from '../components/HeadTab'
import PredictionCard from '../components/PredictionCard'

import daly from '../static/daly.png'
import future from '../static/future.png'
import situation from '../static/situation.png'
import yesOrNo from '../static/yesOrNo.png'
import meaning from '../static/mining.png'
import alDaly from '../static/alDaly.png'
import alFuture from '../static/alFuture.png'

import alSituation from '../static/alSituation.png'
import alYesOrNo from '../static/alYesOrNo.png'
import alMining from '../static/alMining.png'
import shirt from '../static/shirt.png'


export const Prediction = () => {
    return (
        <ChakraProvider>
            <Flex direction={'column'} justifyContent={'center'} alignItems={'center'} position={'fixed'} right={0} top={0} left={0} >
                <Tabs variant="unstyled" defaultIndex={1}  >
                    <TabList >
                        <HeadTab text="О ТАРО" url="about" />
                        <HeadTab text="ГЛАВНАЯ" url="" />
                        <HeadTab text="ГАДАНИЯ" url="predictions" isSelected={true} />
                    </TabList>
                </Tabs>
            </Flex>

            <Box fontSize={24} textColor='#EBCA83'  >

                <VStack position={'static'} >
                    <Container margin={'243px'}>
                        <Center >
                            <Stack spacing={2} direction='row' >
                                <PredictionCard img={daly}
                                    head="КАРТА ДНЯ"
                                    text="При помощи расклада Таро на карту дня, вы сможете более подробно узнать о том что ждет вас сегодня,
                                     проанализировать ситуацию и сделать правильный выбор. Наш онлайн расклад составляется на основе одной карты."
                                    alText="КАРТА ДНЯ"
                                    alImg={alDaly} 
                                    url="dayly"/>
                                <PredictionCard img={future}
                                    head="ГАДАНИЕ НА БУДУЮЩИЕ"
                                    text="Таро расклад позволит узнать что ждет вас в будущем, сделать выводы и принять верное решение. 
                                В раскладе присутствуют все 78 арканов, каждый из которых может дать ответ на вашу ситуацию в ближайшем будущем."
                                    alImg={alFuture}
                                    alText="ГАДАНИЕ НА БУДУЮЩИЕ"
                                    url="future" />
                            </Stack>
                        </Center>
                        <Center marginTop={'45px'}>
                            <Stack spacing={2} direction='row'>
                                <PredictionCard img={situation}
                                    head="ГАДАНИЕ НА СИТУАЦИЮ"
                                    text="При помощи гаданий на картах Таро, можно узнать овтет на свою ситуацию или важный вопрос. 
                                    Получив свой расклад с подробной трактовкой, вы сможете сделать выводы и принять верное решение."
                                    alImg={alSituation}
                                    alText="ГАДАНИЕ НА СИТУАЦИЮ" 
                                    url="situation"/>
                                <PredictionCard img={yesOrNo}
                                    head="ГАДАНИЕ: ДА ИЛИ НЕТ"
                                    text="У каждого человека случаются ситуации, в которых ему непросто принять решение и сделать правильный выбор. 
                                    Гадание «да-нет» поможет понять, какой итог случится в будущем."
                                    alImg={alYesOrNo}
                                    alText="ГАДАНИЕ: ДА ИЛИ НЕТ" 
                                    url="yes_or_no"/>
                            </Stack>
                        </Center>
                        <Center marginTop={'45px'}>
                            <Stack spacing={2} direction='row'>
                                <PredictionCard img={meaning}
                                    head="ЗНАЧЕНИЕ И ТОЛКОВАНИЕ"
                                    text="Для точного и правдивого гадания на картах Таро нужно знать подробное значение этих карт. 
                                Стандартная колода состоит из 78 карт, каждая из которых имеет определенное значение как и в прямом, так и в перевернутом положении."
                                    alImg={alMining}
                                    alText="ЗНАЧЕНИЕ И ТОЛКОВАНИЕ" 
                                    url="meaning"/>
                                <Card bg={'#ffffff00'}
                                    borderColor={'#EBCA83'}
                                    borderRadius={'51.5px'}
                                    width={'560px'}
                                    height={'299px'}
                                    borderWidth={1}
                                    direction={'row'}
                                    marginRight={'51px'}
                                    position={'static'}>
                                    <CardBody backgroundImage={shirt} backgroundSize={'contain'} >
                                        <Center alignItems={'center'} alignContent={'center'} marginTop={'50px'} >
                                            <Text fontFamily={'Cormorant Infant'} textColor={'#EBCA83'} align={'center'} fontSize={32} as='b'>
                                                Совет:
                                            </Text>
                                        </Center>
                                        <Center >
                                            <Text fontFamily={'Cormorant Unicase'} textColor={'#EDF2F4'} align={'center'} fontSize={20}>
                                                Не рекомендуем слишком часто пользоваться Таро раскладами, так как это может ввести вас в заблуждение.
                                            </Text>
                                        </Center>
                                    </CardBody>
                                </Card>
                            </Stack>
                        </Center>
                    </Container>
                </VStack>

                <Footer />

            </Box>

        </ChakraProvider>
    )
}