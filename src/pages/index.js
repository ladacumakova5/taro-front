import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Home } from './Home'
import { Prediction } from './Prediction'
import { Predict } from './Predict'
import {About} from './About'

export const Pages = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/predictions" element={<Prediction/>}/>
                <Route path='/predictions/dayly' element={<Predict textHead="ГАДАНИЕ НА КАРТУ ДНЯ"
                text="Внимательно сосредоточьтесь на предстоящем дне, подумайте о том что бы вы хотели узнать, 
                затем выберите карту из колоды. Внизу будет представлена интерпретация вашего расклада."/>}/>
                <Route path='/about' element={<About/>}/>
            </Routes>
        </BrowserRouter>
    )
}