import { ChakraProvider, Box, Flex, Tabs, TabList, Stack, Center, Container, VStack, CardBody, Card, Text } from '@chakra-ui/react'
import HeadTab from '../components/HeadTab'
import {ChoosingCard} from '../components/ChoosingCard'
import Footer from '../components/Footer'


export const Predict = (props) => {
    return (
        <ChakraProvider>
            <Flex direction={'column'} justifyContent={'center'} alignItems={'center'} position={'fixed'} right={0} top={0} left={0} >
                <Tabs variant="unstyled" defaultIndex={1} w={'25%'} >
                    <TabList >
                        <HeadTab text="О ТАРО" url="about" />
                        <HeadTab text="ГЛАВНАЯ" url="" />
                        <HeadTab text="ГАДАНИЯ" url="predictions" isSelected={true} />
                    </TabList>
                </Tabs>
            </Flex>
            <Box fontSize={24} textColor='#EBCA83' h={'70%'}>
                <VStack>
                    <Container centerContent fontSize={40} as={'b'} marginTop={'10%'} fontFamily={'Cormorant Infant'} textAlign={'center'}>{props.textHead}</Container>
                    <Container centerContent fontSize={24} textColor={'#4B2466'} textAlign={'center'} fontFamily={'Cormorant Unicase'} borderRadius={'72px'} bg={'#E7C06E'}>
                        {props.text}
                    </Container>
                </VStack>
                <Center>
                    <ChoosingCard image="https://i.pinimg.com/236x/48/a6/86/48a686617fbb63105a3a269d10e6adf1.jpg"
                    head="ПАЖ МЕЧЕЙ"
                    text="Это карта перспектив, чреватых осложнениями, испытаний, коварства и ненадежности. Предвестник неприятных, неожиданных новостей, конфликтов, хитрости и эгоизма. Вероятны провокации ловкого соперника, ненадежного соратника, который в ключевой момент провалит дело. "/>
                </Center>
            </Box>
            <Box marginTop={'5%'}>
            <Footer/></Box>
        </ChakraProvider>
    )
}