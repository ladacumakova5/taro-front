import {
    Center, Text, Stack, Tabs,
    TabList,
    Box, Container, VStack, Flex, ChakraProvider, Card, CardBody, Heading,
    Accordion,
    AccordionButton,
    AccordionItem,
    AccordionPanel,
    UnorderedList,
    ListItem
} from '@chakra-ui/react'
import image1 from '../static/image-from-rawpixel-id-6434122-original 1I331_22;248_298.png'
import img2 from '../static/image-from-rawpixel-id-6434112-original 1I331_14;250_299.png'
import img3 from '../static/image-from-rawpixel-id-6434103-original 1I331_32;250_300.png'
import shirt from '../static/image 8.png'
import '../index.css'
import '@fontsource/cormorant-unicase';
import '@fontsource/cormorant-infant'

import HeadTab from '../components/HeadTab'
import { MainCard } from '../components/MainCard'
import ButtonRedirect from '../components/ButtonRedirect'
import Footer from '../components/Footer'

import bg from '../static/rect.png'


export const About = () => {
    return (
        <ChakraProvider  >
            <Flex direction={'column'} justifyContent={'center'} alignItems={'center'} position={'fixed'} right={0} top={0} left={0} >
                <Tabs variant="unstyled" defaultIndex={1}  >
                    <TabList >
                        <HeadTab text="О ТАРО" url="about" isSelected={true} />
                        <HeadTab text="ГЛАВНАЯ" url="" />
                        <HeadTab text="ГАДАНИЯ" url="predictions" />
                    </TabList>
                </Tabs>
            </Flex>
            <Stack margin={'200'} textColor={'white'} fontFamily={'Cormorant Unicase'} fontSize={'24px'}>
                <Accordion >
                    <AccordionItem borderTopRadius={'50'}>
                        <AccordionButton borderTopRadius={'50'} padding={0} border={3} borderColor={"#9747FF"} bgImage={bg} bgSize={
                            'cover'
                        } color={"#4B2466"} _expanded={{ color: "#EBCA83", bg: "#EBCA8300" }}
                        _hover={{bgImage:{bg} ,bgSize:'cover' }}>
                            <Center as="span" flex='1' textAlign='left'>
                                <Heading fontSize={'40px'} fontFamily={'Cormorant Infant'}>
                                    ИСТОРИЯ ПОЯВЛЕНИЯ КАРТ ТАРО
                                </Heading>
                            </Center>
                        </AccordionButton>
                        <AccordionPanel pb={4}>
                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}> Основные версии происхождения:</Heading>

                            <UnorderedList>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>
                                        Египетское происхождение. </Heading>
                                    Согласно этой теории, магические карты являются тайными зашифрованными знаниями жрецов Древнего Египта.</ListItem>
                                <ListItem><Heading textColor={"#EBCA83"} fontSize={'24px'}>Цыганская версия.</Heading> Поклонники этой легенды утверждают, что о картах Таро мир узнал от кочевых племен — цыган.</ListItem>
                                <ListItem><Heading textColor={"#EBCA83"} fontSize={'24px'}>Загадочная Атлантида. </Heading>Версия говорит, что магическая колода — это знания о мире, которые достались нам от исчезнувшей цивилизации атлантов.
                                </ListItem>
                                <ListItem><Heading textColor={"#EBCA83"} fontSize={'24px'}>Каббалистическая версия.</Heading> Согласно этому предположению карты Таро были придуманы евреями, поскольку структура магической колоды тесно связана с Деревом Сефиротом.
                                </ListItem>
                            </UnorderedList>


                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>
                                Однако историки карт Tapo считают, что знакомая нам колода Таро возникла на территории Северной Италии.</Heading>
                            Само слово «Tapo» — это итальянское «Tarocci», отображающее древние версии этой колоды

                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>
                                Первым Tapo и эзотерику возвел макон Антуан Кур де Жебелен, уважавший научные исследования и оккультизм.
                            </Heading >
                            В своих работах он описывал идею о том, что древним людям удавалось жить гораздо лучше и качественнее, чем современным.

                            Через три года после выхода работы де Жебелена французский предсказатель Жан-Батист Альетте под псевдонимом Эттейлла выпустил колоду Таро и первую инструкцию по гаданиям.

                            В 1804 году Мельхиор Монтминьон д’Одюсье, ученик Эттейлы, организовал новое издание полного набора карт, которое он назвал «Таро Гранд Эттейла».

                        </AccordionPanel>
                    </AccordionItem>

                    <AccordionItem>
                        <AccordionButton bgImage={bg} bgSize={
                            'cover'
                        } color={"#4B2466"} _expanded={{ color: "#EBCA83", bg: "#EBCA8300" }} _hover={{bgImage:{bg} ,bgSize:'cover' }}>
                            <Center as="span" flex='1' textAlign='left'>
                                <Heading fontSize={'40px'} fontFamily={'Cormorant Infant'} >
                                    ВИДЫ КАРТ ТАРО</Heading>
                            </Center>
                        </AccordionButton>

                        <AccordionPanel pb={4}>
                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>
                                Популярные колоды Таро:</Heading>
                            <UnorderedList>
                                <ListItem><Heading textColor={"#EBCA83"} fontSize={'24px'}>Марсельское таро</Heading> — одна из старейших колод, дизайн сохранился с XVIII века, изобилует контрастными цветами и приближена к классическим игровым наборам;
                                </ListItem>
                                <ListItem><Heading textColor={"#EBCA83"} fontSize={'24px'}> Таро Райдера-Уэйта</Heading> — 11 и 8 элементы Старшего Аркана — Правосудие и Сила — поменяны местами, карты изобилуют оккультной символикой, изображения реалистичны и ярки;
                                </ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>
                                        Таро Алистер Кроули</Heading> — толкование представленное в Книге Тота, набор наполненный мистическими картинами и фантазийными существами в темных тонах;

                                </ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>
                                        Гранд Эттейла Таро</Heading> — пропитанная духом мистики и оккультизма, исполненная в темных приглушенных тонах, по утверждениям автора идея получена из сновидений;

                                </ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>Колода Висконти </Heading> — Сфорцы. Именно в ней формируется символическая система Таро. Она очевидное порождение североитальянской культуры эпохи Возрождения.

                                </ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>
                                        Сюрреалистические карты Таро от Сальвадора Дали.</Heading> Колода карт Таро, которую ему заказали для фильма о Джеймсе Бонде «Живи и дай умереть», первой ленте с Роджером Муром в главной роли. Однако эти карты так и не появились в картине. Несмотря на это, Дали продолжил работать над колодой Таро, и закончил её спустя почти десять лет — в 1984 году.

                                </ListItem>
                            </UnorderedList>
                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>
                                Каждая колода имеет отличия, связанные с философскими и эзотерическими взглядами их творцов.
                            </Heading>
                            В любом случае все эти разновидности можно использовать для предсказаний и получения ответов на интересующие вопросы. А поскольку в этих картах использована символика и принципы астрологии, каббалы, нумерологии и других древних знаний, сложно даже представить, какие тайны и знания зашифровали в них наши предки.-
                        </AccordionPanel>
                    </AccordionItem>
                    <AccordionItem borderBottomRadius={'50'}>
                        <AccordionButton bgImage={bg} bgSize={
                            'cover'
                        } color={"#4B2466"} _expanded={{ color: "#EBCA83", bg: "#EBCA8300" } }_hover={{bgImage:{bg} ,bgSize:'cover' }} borderBottomRadius={'50'}>
                            <Center as="span" flex='1' textAlign='left'>
                                <Heading fontSize={'40px'} fontFamily={'Cormorant Infant'}>
                                    КАК УСТРОЕНА КОЛОДА КАРТ ТАРО
                                </Heading>
                            </Center>
                        </AccordionButton>

                        <AccordionPanel pb={4}>
                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>
                                Таро – это четкая сформированная система.</Heading>
                            В колоде всегда 78 карт с очень конкретными значениями.
                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>
                                Старшие Арканы Таро</Heading> состоят из 22 карт, которые символически представляют важные жизненные события или архетипические энергии.

                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>Младшие</Heading> 56 карт разделены на четыре масти, представляющие элементы Земли, Воздуха, Огня и Воды – соответственно, Пентакли, Мечи, Жезлы, и Чаши. В каждой масти – четыре карты Двора (Паж, Рыцарь, Королева, Король) и десять номерных карт (от Туза до Десятки). Они символизируют события, ситуации и прочий «жизненный фон». В разных колодах названия как Старших, так и Младших Арканов могут отличаться, но значения останутся прежними.

                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}> Также стоит отметить, что бывают колоды, где к основным добавлены еще одна-две дополнительные карты.</Heading> Часто это «пустые» черные или белые карты или карта Мастера. Это не имеет большого значения для гадания и сделано, только исходя из личного видения создателя колоды. Систему это никак не меняет.

                            <Heading fontSize={'36px'} fontFamily={'Cormorant Infant'} textColor={"#EBCA83"}>Преимущества использования универсальной колоды Таро включают:</Heading>

                            <UnorderedList>
                                <ListItem><Heading textColor={"#EBCA83"} fontSize={'24px'}>Гибкость.</Heading> Она позволяет адаптироваться к различным методам чтения и системам интерпретации.</ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}> Всесторонность.</Heading> Карты охватывают широкий спектр тематик, что позволяет использовать их для разнообразных вопросов и областей жизни.

                                </ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>Интуитивность.</Heading> Символика карт обычно наглядна и понятна, что делает их доступными даже для начинающих.

                                </ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>Универсальные значения.</Heading> Карты могут быть использованы в контексте разных культур и традиций, предоставляя общие архетипические значения.

                                </ListItem>
                                <ListItem>
                                    <Heading textColor={"#EBCA83"} fontSize={'24px'}>Креативность.</Heading> Универсальная колода Таро может вдохновить креативность и интуитивное мышление, способствуя развитию собственного стиля чтения и работы с картами.

                                </ListItem>
                            </UnorderedList>


                        </AccordionPanel>
                    </AccordionItem>
                </Accordion>
            </Stack>
            <Footer/>
        </ChakraProvider>)
}