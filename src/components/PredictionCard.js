import React from 'react'
import { Card, Image, CardBody, Heading, Text, Center, useBoolean } from '@chakra-ui/react'
import { useNavigate } from "react-router-dom";

export default function PredictionCard(props) {
    const [flag, setFlag] = useBoolean()
    const navigate = useNavigate();

    return (
        <Card bg={'#ffffff00'}
            borderColor={'#EBCA83'}
            borderRadius={'51.5px'}
            width={'560px'}
            height={'299px'}
            borderWidth={1}
            direction={'row'}
            marginRight={'51px'}
            position={'static'}
            onMouseEnter={setFlag.on} onMouseLeave={setFlag.off}
            onClick={async event => { navigate(`/predictions/${props.url}`) }}>
            {!flag ? <Center>
                <Image src={props.img} h={'258px'} w={'150px'} marginLeft={'4'} />
                <CardBody>
                    <Heading fontFamily={'Cormorant Infant'} textColor={'#EBCA83'} fontSize={24} as={'b'}>{props.head}</Heading>
                    <Text fontFamily={'Cormorant Unicase'} textColor={'#EDF2F4'} fontSize={20}>{props.text}</Text>
                </CardBody>
            </Center>
                : <CardBody bgImage={props.alImg} backgroundSize={'cover'} borderWidth={9} borderColor={'#EBCA83'} borderRadius={'51.5px'}>
                    <Center fontFamily={'Cormorant Infant'} textColor={'#EBCA83'} fontSize={64} as={'b'} align={'center'} h={'240px'}>
                        {props.alText}
                    </Center>
                </CardBody>}

        </Card>
    )
}