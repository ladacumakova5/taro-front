import React from "react";
import { Card, CardBody, Text, Center , Image} from "@chakra-ui/react";
import shirt from '../static/image 8.png'

import card from '../static/card.png'


export class MainCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isToggleOn: true };

        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
        }));
    }

    render() {
        return (
            <Card height={'535px'} width={'385px'} position={'static'} bg={'#ffffff00'} border={'#ffffff00'} shadow={'none'} onClick={this.handleClick}  marginRight={'168px'}>

                {this.state.isToggleOn ?
                    <CardBody backgroundImage={card} backgroundSize={'contain'}>
                        <Center>
                            <Image src={this.props.image} h={this.props.imgH} />
                        </Center>
                        <Center >
                            <Text fontFamily={'Cormorant Infant'} textColor={'#EBCA83'} align={'center'} fontSize={24} as={'b'}>
                                {this.props.textHead}
                            </Text>
                        </Center>
                        <Center>
                            <Text fontFamily={'Cormorant Unicase'} textColor={'#EDF2F4'} align={'center'} fontSize={20}>
                                {this.props.textMini}
                            </Text>
                        </Center>
                    </CardBody> :
                    <CardBody backgroundImage={shirt} backgroundSize={'cover'} height={'535px'} width={'400px'}>
                        <Center alignItems={'center'} alignContent={'center'}  marginTop={'136px'}>
                            <Text fontFamily={'Cormorant Infant'} textColor={'#EBCA83'} align={'center'} fontSize={24} as={'b'}>
                                {this.props.alTextHead}
                            </Text>
                        </Center>
                        <Center>
                            <Text fontFamily={'Cormorant Unicase'} textColor={'#EDF2F4'} align={'center'} fontSize={20}>
                                {this.props.alTextMini}
                            </Text>
                        </Center>
                    </CardBody>}

            </Card>
        )
    }
}