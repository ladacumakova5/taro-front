import React from "react";
import { Image, Box, Text, Stack,  Heading, Center, VStack, Button, } from "@chakra-ui/react"
import shit from '../static/image 8.png'
import ButtonRedirect from "./ButtonRedirect";

export class ChoosingCard extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isToggleOn: true };
        

        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.setState(prevState => ({
            isToggleOn: !prevState.isToggleOn
        }));
    }

    render() {
        return (
            <Box marginTop={'5%'}>
                {this.state.isToggleOn ?
                    <Box>
                        <VStack>
                            <Image src={shit} h={300} onClick={this.handleClick} />
                            <Text color={'#EDF2F4'} fontFamily={'Cormorant Infant'} fontSize={32} as={'b'} >НАЖМИ НА КАРТУ</Text>
                        </VStack>
                    </Box> :
                    <Center>
                        <Stack>
                            <Text>Это пример</Text>
                            <Center>
                                
                                <Image src={this.props.image} h={300} />
                                <VStack marginLeft={10}>
                                    <Heading fontFamily={'Cormorant Infant'} textColor={'#EBCA83'} fontSize={24} as={'b'} w={400}>{this.props.head}</Heading>
                                    <Text fontFamily={'Cormorant Unicase'} textColor={'#EDF2F4'} fontSize={20} w={400}>{this.props.text}</Text>
                                </VStack>

                            </Center>
                            
                        </Stack>
                    </Center>}



            </Box>
        )
    }
}