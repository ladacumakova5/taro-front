import React from "react";
import { Text, Center, Image , Box} from "@chakra-ui/react";
import logo from "../static/logo.png"

export default function Footer(props) {
    return (
        <Box>
        <Center>
            <Image src={logo} marginTop={'20px'} height={'171px'}/>
        </Center>
        <Center>
            <Text fontFamily={'Cormorant Unicase'} fontSize={'20px'} textColor={'#EDF2F4'} marginBottom={'50'} as={'b'}>2023</Text>
        </Center>
        </Box>
    )
}