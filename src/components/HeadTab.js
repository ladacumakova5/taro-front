import React from "react";
import { useNavigate } from "react-router-dom";
import { chakra, Text } from "@chakra-ui/react";

export default function HeadTab(props) {

    const navigate = useNavigate();
    if (!props.isSelected) {
        return (

            <chakra.tab
                bg='#4D236A'
                textColor='#EBCA83'
                borderColor='#EBCA83'
                borderWidth={2}
                fontFamily='Cormorant Infant'

                borderBottomRadius="2xl" as={'b'} height={'45px'} marginEnd={"37px"} fontSize={24} width='151px'

                _hover={{
                    bg: '#E7C06E', textColor: '#4D236A', fontFamily: 'Cormorant Infant', height: '55px'
                }}
                onClick={async event => { navigate(`/${props.url}`) }}

            >
                <Text align={'center'}   >
                    {props.text}
                </Text>
            </chakra.tab>)
    } else {
        return (

            <chakra.tab
                borderColor='#EBCA83'
                borderWidth={2}

                bgGradient='linear(to-tr, #FDFAFA, #EBCA83, #4D236A)'
                textColor='#4B2466'
                fontFamily='Cormorant Infant'
                borderBottomRadius="2xl" as={'b'} height={'50px'} marginEnd={"37px"} fontSize={24} top='-9px' width='151px'

                _hover={{
                    bg: '#E7C06E', textColor: '#4D236A', fontFamily: 'Cormorant Infant', height: '55px'
                }}
                onClick={async event => { navigate(`/${props.url}`) }}

            >
                <Text align={'center'}   >
                    {props.text}
                </Text>
            </chakra.tab>)
    }

}