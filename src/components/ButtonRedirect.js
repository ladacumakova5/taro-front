import React from "react";
import { useNavigate } from "react-router-dom";
import { Button} from "@chakra-ui/react";

export default function ButtonRedirect(props) {
    const navigate = useNavigate();
    return (
        <Button width={'476px'} 
        height={'107px'} 
        borderRadius={'61px'} 
        bg='#EBCA8300' 
        borderColor='#EBCA83' 
        borderWidth={2} 
        fontFamily={'Cormorant Infant'}
        fontSize={24} 
        _hover={{
            bgGradient:'linear(to-tr, #FDFAFA, #EBCA83, #4D236A)',
            textColor:'#4B2466'
        }}
        onClick={async event => { navigate(`/${props.url}`) }}>
            {props.text}
        </Button>
    )
}